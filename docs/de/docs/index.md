# JTL-Shop Entwickler-Dokumentation

Diese Dokumentation richtet sich vorwiegend an Entwickler von *Plugins* und *Templates* für JTL-Shop und ambitionierte
Programmierer, die sich an der Weiterentwicklung von JTL-Shop beteiligen möchten.

Grundlegende Programmierkenntnisse werden vorausgesetzt.

![JTL Shop development documentation](_images/logo.png)
