# HOOK_BOXEN_INC_ERSCHEINENDEPRODUKTE (86)

## Triggerpunkt

Vor der Anzeige, der neu erscheinenden Artikel

## Parameter

* `JTL\Boxes\Items\UpcomingProducts` **&box** - Objekt für neu erscheinende Artikel
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"