# HOOK_REGISTRIEREN_PAGE_REGISTRIEREN_PLAUSI (41)

## Triggerpunkt

Plausibilitätsprüfung nach dem Abschicken einer Kundenregistrierung

## Parameter

* `int` **&nReturnValue** - Resultat der Plausibilitätsprüfung
* `array` **&fehlendeAngaben** - Liste der nicht ausgefüllten Formularfelder