# HOOK_BOXEN_INC_NEUIMSORTIMENT (83)

## Triggerpunkt

Vor der Anzeige der, neu im Sortiment befindlichen, Artikel

## Parameter

* `JTL\Boxes\Items\NewProducts` **&box** - "Neu-im-Sortiment"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"