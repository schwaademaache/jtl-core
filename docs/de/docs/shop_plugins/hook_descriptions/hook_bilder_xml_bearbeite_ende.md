# HOOK_BILDER_XML_BEARBEITE_ENDE (179)

## Triggerpunkt

Nach dem Holen der Bilderliste (in dbeS)

## Parameter

* `array` **&Artikel** - Liste der Artikelbilder
* `array` **&Kategorie** - Liste der Kategoriebilder
* `array` **&Eigenschaftswert** - Liste der Eigenschaftsbilder
* `array` **&Hersteller** - Liste der Herstellerbilder
* `array` **&Merkmalwert** - Liste der Merkmalwertbilder
* `array` **&Merkmal** - Liste der Merkmalbilder
* `array` **&Konfiggruppe** - Liste der Konfiggruppenbilder