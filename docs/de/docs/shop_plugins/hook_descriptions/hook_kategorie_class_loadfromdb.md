# HOOK_KATEGORIE_CLASS_LOADFROMDB (120)

## Triggerpunkt

Nach dem Laden einer Kategorie aus der Datenbank

## Parameter

* `JTL\Catalog\Category\Kategorie` **&oKategorie** - Kategorie-Objekt
* `array` **cacheTags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"