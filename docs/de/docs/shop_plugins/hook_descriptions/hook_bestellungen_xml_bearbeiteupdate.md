# HOOK_BESTELLUNGEN_XML_BEARBEITEUPDATE (209)

## Triggerpunkt

Nach dem Update einer Bestellung in der Datenbank (in dbeS)

## Parameter

* `stdClass` **&oBestellung** - Objekt der neuen Bestellung
* `stdClass` **&oBestellungAlt** - Objekt der alten Bestellung
* `JTL\Customer\Kunde` **&oKunde** - Kundenobjekt