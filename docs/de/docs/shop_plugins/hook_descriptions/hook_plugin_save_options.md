# HOOK_PLUGIN_SAVE_OPTIONS (280)

## Triggerpunkt

Nach dem Speichern der Plugin-Optionen

## Parameter

* `JTL\Plugins\PluginInterface` **plugin** - Pluginobjekt
* `bool` **&hasError** - Flag, welches anzeigt, ob das Plugin fehlerfrei geladen werden konnte
* `string` **&msg** - Meldung, die beim Laden aufgetreten ist
* `string` **error** - (ab Version 5.0) Fehlermeldung
* `array` **options** - Liste von Konfigurationsoptionen