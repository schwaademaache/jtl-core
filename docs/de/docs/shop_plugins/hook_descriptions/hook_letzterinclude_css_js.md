# HOOK_LETZTERINCLUDE_CSS_JS (175)

!!! role strike
    :class: strike

Vor dem Hinzufügen von CSS/JS zum Shop-Smarty-Objekt

## Parameter

* `array` **&cCSS_arr** - Template-CSS
* `array` **&cJS_arr** - Template-JS
* `array|mixed` **&cPluginCss_arr** - Plugin-CSS
* `array|mixed` **&cPluginJsHead_arr** - Plugin-JS für den Head
* `array|mixed` **&cPluginJsBody_arr** - Plugin-JS für den Body